﻿using System;
using System.IO;
using System.Collections.Generic;
using TeamHeroCoderLibrary;

namespace PlayerCoder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PartyAIManager partyAIManager = new MyAI();
            partyAIManager.SetExchangePath("Paste here the file exchange directory provided by Team Hero Coder");
            partyAIManager.StartReadingAndProcessingInfiniteLoop();
        }
    }

    public class MyAI : PartyAIManager
    {
        public override void ProcessAI()
        {
            Console.WriteLine("Processing AI!");

            #region SampleCode

            if (TeamHeroCoder.BattleState.heroWithInitiative.characterClassID == TeamHeroCoder.HeroClassID.Fighter)
            {
                //The character with initiative is a figher, do something here...

                Console.WriteLine("this is a fighter");
            }
            else if (TeamHeroCoder.BattleState.heroWithInitiative.characterClassID == TeamHeroCoder.HeroClassID.Cleric)
            {
                //The character with initiative is a figher, do something here...

                Console.WriteLine("this is a cleric");
            }
            else if (TeamHeroCoder.BattleState.heroWithInitiative.characterClassID == TeamHeroCoder.HeroClassID.Wizard)
            {
                //The character with initiative is a figher, do something here...

                Console.WriteLine("this is a wizard");
            }

            foreach(InventoryItem ii in TeamHeroCoder.BattleState.playerInventory)
            {
                
            }

            //Find the foe with the lowest health
            Hero target = null;

            foreach (Hero hero in TeamHeroCoder.BattleState.foeHeroes)
            {
                if (hero.health > 0)
                {
                    if (target == null)
                        target = hero;
                    else if (hero.health < target.health)
                        target = hero;
                }
            }

            //This is the line of code that tells FG that we want to perform the attack action and target the foe with the lowest HP
            TeamHeroCoder.PerformHeroAbility(TeamHeroCoder.AbilityID.Attack, target);

            //Searching for a poisoned hero 
            foreach (Hero hero in TeamHeroCoder.BattleState.playerHeroes)
            {
                foreach (StatusEffect se in hero.statusEffects)
                {
                    if (se.id == TeamHeroCoder.StatusEffectID.Poison)
                    {
                        //We have found a character that is poisoned, do something here...
                    }
                }
            }

            #endregion
        
        }
    }

}
